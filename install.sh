#!/bin/sh

###########################################
#       @Author: Vladimir Sukhov          #
#       @Software: atlaskeepr             #
#       @License: MIT                     #
#  @Year, Place: 2018 Brisbane, Australia #
###########################################


SRCDIR="src/"
BUILDDIR="bin/"
FNAME="ak"
ENTRY="main.go"

#For windows specify
# GOOS="windows"
# GOARCH="386" || GOARCH="amd64" depending on an architecture

GOOS=
GOARCH=
CFLAGS="${GOOS}-${GOARCH}"
size=${#CFLAGS}


if [ $size -gt 1 ] 
    then
        echo "Exporting ${CFLAGS}"
        export "GOOS=${GOOS}"
        export "GOARCH=${GOARCH}"

        if [ $GOOS == "windows" ] 
            then
                FNAME="windows/ak.exe"
        fi

else
    echo "Building for default architecture"
fi

DEFOUT="${BUILDDIR}${FNAME}"

go build -o ${DEFOUT} ${SRCDIR}${ENTRY}

if [ -z "$1" ]
  then
    echo "Skipping install, please manually copy the binary"
elif [ "$1" == "install" ]
    then
        echo "Installing..."
        cp ${BUILDDIR}${FNAME} /usr/local/bin/
else
    echo "unknown argument $1, did you mean install?"
fi

echo "All done..."