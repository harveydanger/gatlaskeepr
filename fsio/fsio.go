package fsio

/*
@Author: Vladimir Sukhov
@Software: atlaskeepr
@License: MIT
@Year, Place: 2018 Brisbane, Australia
*/

import (
	"bufio"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"syscall"

	"golang.org/x/crypto/ssh/terminal"

	"gatlaskeepr/model"
)

// CompositePath - absolute path of the ak directory
var CompositePath string

var mod model.Models

const (
	akpath  = "/.ak/"
	phName  = "atlaskeepr"
	phValue = "genuine"
)

// SetPath - set the absolute path to the ak directory
func SetPath() bool {

	homeDir := os.Getenv("HOME")

	if len(homeDir) == 0 {
		log.Println("Unable to get home directory. Please specify $HOME in your PATH")
		return false
	}

	CompositePath = os.Getenv("HOME") + akpath

	directoryExists()

	return true
}

// ListAllVaults - lists directory contents (ak)
func ListAllVaults() {
	files, err := ioutil.ReadDir(CompositePath)

	if err != nil {
		log.Fatalln("Could not read from atlaskeepr directory:", err)
	}

	for _, f := range files {
		fmt.Println(f.Name())
	}
}

// MakeNewVault - create new vault
func MakeNewVault(name string) {
	fullPath := CompositePath + name
	e := vaultExists(name)
	if !e {
		file, err := os.Create(fullPath)
		defer file.Close()
		if err != nil {
			log.Fatalln("Couldn't create a vault. Terminating:", err)
		}

		var m model.Model
		m.Name = phName
		m.Value = phValue

		mod.Models = append(mod.Models, m)

		password := string(readPasswordFromSTDIN())

		putVaultContents(name, password)

		fmt.Println("Vault", name, "has been successfully created")
	} else {
		fmt.Println("Specified vault already exists")
	}

}

// RemoveVault - remove specified vault from filesystem
func RemoveVault(name string) {
	ex := vaultExists(name)

	if !ex {
		fmt.Println("Vault does not exist")
	} else {
		err := os.RemoveAll(CompositePath + name)

		if err != nil {
			log.Fatal("Could not remove vault:", err)
		}

		fmt.Println("Vault", name, "has been removed")
	}
}

// ListAllNodes - list entire list of nodes from a vault
func ListAllNodes(name string) {

	if !vaultExists(name) {
		fmt.Println("Vault", name, "doesn't exist")
		os.Exit(0)
	}

	password := string(readPasswordFromSTDIN())
	getVaultContents(name, password)
	for _, k := range mod.Models {
		if k.Name != "atlaskeepr" {
			fmt.Println("Name:", k.Name, "Value:", k.Value)
		}
	}
}

// GetNodeFromVault - retrieve node password by name
func GetNodeFromVault(name string, nodeName string) {
	if !vaultExists(name) {
		fmt.Println("Vault", name, "doesn't exist")
		os.Exit(0)
	}

	password := string(readPasswordFromSTDIN())
	getVaultContents(name, password)
	found := false
	for _, k := range mod.Models {
		if k.Name == nodeName {
			fmt.Println(k.Value)
			found = true
		}
	}
	if !found {
		fmt.Println("Node", nodeName, "is not found in", name)
	}

}

// AddNode - adds node to the specified vault
func AddNode(name string, nodeName string, password string) {
	nodePassword := password
	if !vaultExists(name) {
		fmt.Println("Vault", name, "doesn't exist")
		os.Exit(0)
	}
	vp := string(readPasswordFromSTDIN())
	getVaultContents(name, vp)
	for _, k := range mod.Models {
		if k.Name == nodeName {
			fmt.Println("Node", nodeName, "already exists")
			os.Exit(0)
		}
	}
	if nodePassword == "" {
		nodePassword = string(readNodePasswordFromSTDIN())
	}

	m := model.Model{Name: nodeName, Value: nodePassword}
	mod.Models = append(mod.Models, m)
	putVaultContents(name, vp)
	fmt.Println("Node", nodeName, "has beed successfully added to", name, "vault with key:", nodePassword)
}

// RemoveNodeFromVault - removes specified node from a vault
func RemoveNodeFromVault(name, nodeName string) {
	if !vaultExists(name) {
		fmt.Println("Vault", name, "doesn't exist")
		os.Exit(0)
	}

	password := string(readPasswordFromSTDIN())
	getVaultContents(name, password)
	found := false
	for i, k := range mod.Models {
		if k.Name == nodeName {
			mod.Models = append(mod.Models[:i], mod.Models[i+1:]...)
			found = true
		}
	}
	if found {
		fmt.Println("Node", nodeName, "has been removed from", name)
		putVaultContents(name, password)
	} else {
		fmt.Println("Node", nodeName, "was not found in", name)
	}

}

// getVaultContents - wrapper for reading / decoding contents of the vault
func getVaultContents(name string, password string) {
	data := readVault(name)
	password = packPassword(password)
	contents := decrypt(data, password)
	if contents != nil {
		parseJSON(contents)
	}
}

// putVaultContents - wrapper for encoding / writing contents of the vault
func putVaultContents(name string, password string) {
	contents := getJSON(mod)
	password = packPassword(password)
	data := encrypt(contents, password)
	writeVault(name, data)
}

func getJSON(m model.Models) []byte {
	a, err := json.Marshal(&m)
	if err != nil {
		log.Fatalln("Could not prepare data for encryption", err)
	}
	return a
}

func readPasswordFromSTDIN() []byte {
	fmt.Print("Enter Vault Password: ")
	bytePassword, err := terminal.ReadPassword(int(syscall.Stdin))
	if err != nil {
		log.Fatalln("Could not read from vault.", err)
	}
	fmt.Println()
	return bytePassword
}

func readNodePasswordFromSTDIN() []byte {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter node password: ")
	text, _ := reader.ReadString('\n')
	fmt.Println()
	text = text[0 : len(text)-1] // cut trailing \n
	return []byte(text)
}

func parseJSON(data []byte) {

	if json.Valid(data) {

		e := json.Unmarshal(data, &mod)
		if e != nil {
			log.Fatalln("Strange vault..hmmm..terminating")
		}

	} else {
		fmt.Println("Wrong password supplied.")
	}

}

func packPassword(password string) string {

	newPassword := password
	for {
		newPassword += "!"
		if len(newPassword)%32 == 0 {
			break
		}
	}
	return newPassword

}

func decrypt(json []byte, key string) []byte {

	block, err := aes.NewCipher([]byte(key))
	if err != nil {
		log.Fatalln("BAD fsio 200", err)
	}
	if len(json) < aes.BlockSize {
		log.Fatalln("\nProvided password is too short, that is odd though", err)
	}
	iv := json[:aes.BlockSize]
	json = json[aes.BlockSize:]
	cfb := cipher.NewCFBDecrypter(block, iv)
	cfb.XORKeyStream(json, json)
	data, err := base64.StdEncoding.DecodeString(string(json))
	if err != nil {
		fmt.Println("Wrong password supplied")
		os.Exit(0)
	}
	return data

}

func encrypt(json []byte, key string) []byte {

	block, err := aes.NewCipher([]byte(key))
	if err != nil {
		log.Fatalln("BAD fsio 100", err)
	}

	b := base64.StdEncoding.EncodeToString(json)
	ciphertext := make([]byte, aes.BlockSize+len(b))

	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		log.Fatalln("BAD fsio 101", err)
	}
	cfb := cipher.NewCFBEncrypter(block, iv)
	cfb.XORKeyStream(ciphertext[aes.BlockSize:], []byte(b))
	return ciphertext

}

func readVault(name string) []byte {

	data, err := ioutil.ReadFile(CompositePath + name)
	if err != nil {
		log.Fatal("Could not read from vault:", err)
	}

	return data

}

func writeVault(name string, contents []byte) {
	fullPath := CompositePath + name

	if e := ioutil.WriteFile(fullPath, contents, os.ModePerm); e != nil {
		log.Fatalln("Could not write file")
	}

}

func directoryExists() {
	_, err := os.Stat(CompositePath)

	if os.IsNotExist(err) {
		createDirectory()
	}

}

func vaultExists(name string) bool {
	r, err := os.Stat(CompositePath + name)

	if err != nil {
		return false
	}
	if len(r.Name()) > 0 {
		return true
	}
	return false
}

func createDirectory() {
	fmt.Println("Default ak directory does not exist, creating...")
	err := os.MkdirAll(CompositePath, os.ModePerm)

	if err != nil {
		log.Println("Unable to create atlaskeepr directory")
		os.Exit(-1)
	}

}
