#  Atlas Keepr (ak) - password generator and encrypted storage
## Description

[![Build Status](https://travis-ci.org/HarveyDanger/gatlaskeepr.svg?branch=master)](https://travis-ci.org/HarveyDanger/gatlaskeepr)

Plain GO secure vault, password generator. Encryption is based on AES algorythm.

Current version can be built on macOs, Linux and Windows (binary 386 arch included in bin folder) (tested on macOS High Sierra, Ubuntu 18.04)

## Installation
***

Compile [Windows readme](https://github.com/HarveyDanger/gatlaskeepr/tree/master/bin/windows) (suggested name for app - ak) and copy binary to `/usr/local/bin` . 
or
```
$:/> ./install.sh install
```
*see install.sh for details*

**

## Usage
***
```
ak 
-l to list all vaults
-cv <vault name> to create vault
-rm <vault name> to remove vault
-a <node name> -v <vault name> [-gen [#]] to add node, where # - number of symbols to generate (8, 50)
-g <node name> -v <vault name> to get associated value
-rm <node name> -v <vault name> to remove node
-h show this help file
-all -v <vault name> to get all nodes from vault
```
**Caution: when you create a vault, you are asked to provide a password. Keep it safe and remember it, as there is no way to restore this password later**

## Example
***
*Create vault:*
```
$:/> ak -cv myVault # creates vault with the name myVault
```
*Add node to vault*
```
$:/> ak -a ssh -v myVault # adds node "ssh" to the myVault
```
*Add another node with generated password of 15 characters long*
```
$:/> ak -a mail -v myVault -gen 15 # generates password 15 characters and adds it to the vault
```
*Obtain value of node from vault*
```
$:/> ak -g ssh -v myVault # prints associated password
```
**Author: Vladimir Sukhov 2018 MIT License**

