package main

/*
@Author: Vladimir Sukhov
@Software: atlaskeepr
@License: MIT
@Year, Place: 2018 Brisbane, Australia
*/

import (
	"os"

	"gatlaskeepr/fsio"

	"gatlaskeepr/argparser"
)

func main() {
	args := os.Args[1:]
	pathSet := fsio.SetPath() // set global path of the ak folder (creates if does not exist)
	if pathSet {
		argparser.ParseArguments(args)
	}

}
