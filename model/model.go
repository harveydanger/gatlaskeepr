package model

/*
@Author: Vladimir Sukhov
@Software: atlaskeepr
@License: MIT
@Year, Place: 2018 Brisbane, Australia
*/

//Model - vault data model
type Model struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

// Models - an array of models
type Models struct {
	Models []Model `json:"models"`
}
