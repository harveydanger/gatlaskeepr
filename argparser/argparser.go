package argparser

/*
@Author: Vladimir Sukhov
@Software: atlaskeepr
@License: MIT
@Year, Place: 2018 Brisbane, Australia
*/

import (
	"fmt"
	"math/rand"
	"strconv"
	"time"

	"gatlaskeepr/fsio"
)

/*
ak
-l to list all vaults
-cv <vault name> to create vault
-rm <vault name> to remove vault
-a <node name> -v <vault name> [-gen [#]] to add node, where # - number of symbols to generate (8, 50)
-g <node name> -v <vault name> to get associated value
-rm <node name> -v <vault name> to remove node
-h show this help file
-all -v <vault name> to get all nodes from vault
*/

const (
	akpath            = "/.ak"
	defaultGenPassLen = 12
	letters           = "1234567890!@#$%^&*()-_=+qwertyuiop[{]}asdfghjkl;:zxcvbnm,<.>/?QWERTYUIOPASDFGHJKLZXCVBNM"
)

// ParseArguments - takes initial call and validates an input
func ParseArguments(args []string) {

	if len(args) == 1 {
		if args[0] == "-l" {
			fsio.ListAllVaults()
			return
		} else {
			DumpHelp(1)
			return
		}
	} else if len(args) == 2 {
		if args[0] == "-cv" {
			fsio.MakeNewVault(args[1])
			return
		} else if args[0] == "-rm" {
			fsio.RemoveVault(args[1])
		} else {
			DumpHelp(2)
			return
		}
	} else if len(args) == 3 {
		if args[0] == "-all" && args[1] == "-v" {
			fsio.ListAllNodes(args[2])
		} else {
			DumpHelp(3)
			return
		}
	} else if len(args) == 4 {
		if args[0] == "-g" && args[2] == "-v" {
			fsio.GetNodeFromVault(args[3], args[1])
		} else if args[0] == "-rm" && args[2] == "-v" {
			fsio.RemoveNodeFromVault(args[3], args[1])
		} else if args[0] == "-a" && args[2] == "-v" {
			fsio.AddNode(args[3], args[1], "")
		} else {
			DumpHelp(4)
			return
		}
	} else if len(args) == 5 && args[0] == "-a" && args[2] == "-v" && args[4] == "-gen" {
		pass := generatePassword(0)
		fsio.AddNode(args[3], args[1], pass)

	} else if len(args) == 6 && args[0] == "-a" && args[2] == "-v" && args[4] == "-gen" {

		val, err := strconv.Atoi(args[5])
		if err != nil {
			DumpHelp(5)
			return
		}

		pass := generatePassword(val)
		fsio.AddNode(args[3], args[1], pass)

	} else {
		DumpHelp(6)
		return
	}

}

// GeneratePassword - function to randomly generate a password
func generatePassword(length int) string {
	l := length
	if l == 0 {
		l = defaultGenPassLen
	}
	rand.Seed(time.Now().UnixNano())
	b := make([]byte, l)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)

}

// DumpHelp - dumps help information
func DumpHelp(caller int) {
	// fmt.Println(caller)
	fmt.Println("Thanks for using atlaskeepr")
	fmt.Println("Please be aware that if you lose your vault password - data will be lost forever")
	fmt.Println("Usage:")
	fmt.Println("-l to list all vaults")                                                                                  // len 1
	fmt.Println("-cv <vault name> to create vault")                                                                       // len 2
	fmt.Println("-rm <vault name> to remove vault")                                                                       // len 2
	fmt.Println("-a <node name> -v <vault name> [-gen [#]] to add node, where # - number of symbols to generate (8, 50)") // len 4-6
	fmt.Println("-g <node name> -v <vault name> to get associated value")                                                 // len 4
	fmt.Println("-rm <node name> -v <vault name> to remove node")                                                         // len 4
	fmt.Println("-h show this help file")                                                                                 // len 1
	fmt.Println("-all -v <vault name> to get all nodes from vault")                                                       // len 3
}
