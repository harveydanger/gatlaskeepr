package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"testing"

	"gatlaskeepr/argparser"
)

// TestListFunction - ak -l test
func TestHelpFunction(t *testing.T) {

	ethalon := func() {
		argparser.DumpHelp(1)
	}
	h := captureStdout(ethalon)
	fmt.Println(h)

	attempt := func() {
		argparser.ParseArguments([]string{"-h"})
	}

	v := captureStdout(attempt)

	if h != v {
		t.Error("Output differs")
	}

}

func captureStdout(f func()) string {
	old := os.Stdout
	r, w, _ := os.Pipe()
	os.Stdout = w
	f()
	w.Close()
	os.Stdout = old
	var buf bytes.Buffer
	io.Copy(&buf, r)
	return buf.String()
}
